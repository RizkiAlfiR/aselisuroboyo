package com.example.asus.aselisurabaya;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.asus.aselisurabaya.api.ApiService;
import com.example.asus.aselisurabaya.model.MasukModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private Button btnMasuk;
    private EditText etmUsername, etmPassword;
    String susername, spassword;
    ProgressDialog loading;

    RelativeLayout rellay1, rellay2;
    LinearLayout linear3;
    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            rellay1.setVisibility(View.VISIBLE);
            linear3.setVisibility(View.VISIBLE);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        rellay1 = (RelativeLayout) findViewById(R.id.rellay1);
        linear3 = (LinearLayout) findViewById(R.id.linear3);

        handler.postDelayed(runnable, 1000);

        etmUsername=(EditText)findViewById(R.id.etUsername);
        etmPassword=(EditText)findViewById(R.id.etPassword);
        btnMasuk=(Button)findViewById(R.id.btnLogin);
        btnMasuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                susername = etmUsername.getText().toString();
                spassword = etmPassword.getText().toString();
                if (!susername.isEmpty() && !spassword.isEmpty())
                {
                    doSignIn(susername,spassword);
                }else {
                    Toast.makeText(com.example.asus.aselisurabaya.LoginActivity.this, "Data belum terisi", Toast.LENGTH_SHORT).show();
                }

            }
        });

        Button buttonSignup = (Button) findViewById(R.id.btnSignUp);
        buttonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(intent);
            }
        });
    }

    private void doSignIn(String username, String password) {
        loading = new ProgressDialog(com.example.asus.aselisurabaya.LoginActivity.this, R.style.Theme_AppCompat_DayNight_Dialog);
        loading.setMessage("Authenticating..");
        loading.show();

        ApiService.service_post.postLogin("Application/json",username,password).enqueue(new Callback<MasukModel>() {
            @Override
            public void onResponse(Call<MasukModel> call, Response<MasukModel> response) {
                if (response.code() == 200)
                {
                    loading.dismiss();
                    Toast.makeText(com.example.asus.aselisurabaya.LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent dashboard = new Intent(com.example.asus.aselisurabaya.LoginActivity.this, MainActivity.class);
                    startActivity(dashboard);
                    finish();

                }else if(response.code() == 400){
                    Toast.makeText(LoginActivity.this, "Username/Password Salah", Toast.LENGTH_SHORT).show();
                    loading.dismiss();
                }
                else
                {
                    Toast.makeText(com.example.asus.aselisurabaya.LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<MasukModel> call, Throwable t) {

            }
        });
    }

}
