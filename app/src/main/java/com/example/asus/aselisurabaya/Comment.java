package com.example.asus.aselisurabaya;

/**
 * Created by ASUS on 6/25/2018.
 */

public class Comment {
    private int id_comment;
    private int id_wisata;
    private int id_user;
    private String comment;

    public Comment(int id_comment, int id_wisata, int id_user, String comment) {
        this.setId_comment(id_comment);
        this.setId_wisata(id_wisata);
        this.setId_user(id_user);
        this.setComment(comment);
    }


    public int getId_comment() {
        return id_comment;
    }

    public void setId_comment(int id_comment) {
        this.id_comment = id_comment;
    }

    public int getId_wisata() {
        return id_wisata;
    }

    public void setId_wisata(int id_wisata) {
        this.id_wisata = id_wisata;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
