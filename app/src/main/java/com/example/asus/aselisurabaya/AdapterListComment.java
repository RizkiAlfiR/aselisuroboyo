package com.example.asus.aselisurabaya;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ASUS on 6/23/2018.
 */

public class AdapterListComment extends RecyclerView.Adapter<AdapterListComment.ViewHolder> {
    Context context;
    ArrayList<HashMap<String, String>> list_data;

    public AdapterListComment(CommentActivity commentActivity, ArrayList<HashMap<String, String>> list_data) {
        this.context = commentActivity;
        this.list_data = list_data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_comment, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtComment.setText(list_data.get(position).get("comment"));
    }

    @Override
    public int getItemCount() {
        return list_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtComment;

        public ViewHolder(View itemView) {
            super(itemView);

            txtComment = (TextView) itemView.findViewById(R.id.txtViewComment);
        }
    }
}
