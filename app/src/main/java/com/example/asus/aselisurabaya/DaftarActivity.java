//package com.example.lenovo.myapplication;
//
//import android.app.ProgressDialog;
//import android.content.Intent;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import com.example.asus.aselisurabaya.R;
//import com.example.lenovo.myapplication.api.ApiService;
//import com.example.lenovo.myapplication.model.DaftarModel;
//import com.example.lenovo.myapplication.model.MasukModel;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class DaftarActivity extends AppCompatActivity {
//    private Button btnDaftarFinal;
//    private EditText etdNama, etdEmail, etdPassword;
//    String sdnama,sdemail,sdpassword;
//    ProgressDialog loading;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_daftar);
//
//        etdNama=(EditText)findViewById(R.id.etd_nama);
//        etdEmail=(EditText)findViewById(R.id.etd_email);
//        etdPassword=(EditText)findViewById(R.id.etd_password);
//
//        btnDaftarFinal=(Button)findViewById(R.id.btn_daftar_final);
//        btnDaftarFinal.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sdnama= etdNama.getText().toString();
//                sdemail = etdEmail.getText().toString();
//                sdpassword= etdPassword.getText().toString();
//                if (!sdnama.isEmpty() && !sdemail.isEmpty() && !sdpassword.isEmpty())
//                {
//                    doSignUp(sdnama, sdemail, sdpassword);
//                }else {
//                    Toast.makeText(DaftarActivity.this, "data kosong", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//
//    }
//
//    private void doSignUp(String nama, String email, String password) {
//        loading = new ProgressDialog(DaftarActivity.this, R.style.Theme_AppCompat_DayNight_Dialog);
//        loading.setMessage("Authenticating..");
//        loading.show();
//
//        ApiService.service_post.postRegister("Application/json", nama, email, password).enqueue(new Callback<DaftarModel>() {
//            @Override
//            public void onResponse(Call<DaftarModel> call, Response<DaftarModel> response) {
//                if (response.isSuccessful())
//                {
//                    loading.dismiss();
//                    Toast.makeText(DaftarActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    Intent dashboard = new Intent(DaftarActivity.this, MainActivity.class);
//                    startActivity(dashboard);
//                    finish();
//                }
//                else
//                {
//                    Toast.makeText(DaftarActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    loading.dismiss();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<DaftarModel> call, Throwable t) {
//
//            }
//        });
//
//    }
//}
