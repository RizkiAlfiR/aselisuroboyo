package com.example.asus.aselisurabaya;

import android.app.Dialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CommentActivity extends AppCompatActivity {
    private static final int CODE_POST_REQUEST = 1025;

    private RecyclerView lvComment;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;

    ProgressBar progressBar;
    EditText editComment;
    Button buttonSendComment;

    ArrayList<HashMap<String, String>> list_data;

    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        myDialog = new Dialog(this);
        myDialog.setContentView(R.layout.popup_comment);

        String url = "http://192.168.1.100/aselisurabaya3/aselisurabaya/api/apiComment.php?apicall=get_comment_all"; //url API

        lvComment = (RecyclerView) findViewById(R.id.lvComment);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        lvComment.setLayoutManager(llm);

        buttonSendComment = myDialog.findViewById(R.id.btnSendComment);

        requestQueue = Volley.newRequestQueue(CommentActivity.this);

        list_data = new ArrayList<HashMap<String, String>>();

        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response ", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("comment");
                    for (int a = 0; a < jsonArray.length(); a++) {
                        JSONObject json = jsonArray.getJSONObject(a);
                        HashMap<String, String> map = new HashMap<String, String>();
                        //kolom-kolom
                        map.put("id", json.getString("id_comment"));
                        map.put("id_wisata", json.getString("id_wisata"));
                        map.put("id_user", json.getString("id_user"));
                        map.put("comment", json.getString("comment"));
                        map.put("tgl_pembuatan", json.getString("tgl_pembuatan"));
                        list_data.add(map);
                        AdapterListComment adapter = new AdapterListComment(CommentActivity.this, list_data);
                        lvComment.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CommentActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(stringRequest);
    }

    public void ShowPopup(View v) {
        TextView txtClose;
        Button buttonSendComment;

        txtClose = (TextView) myDialog.findViewById(R.id.txtClose);
        buttonSendComment = (Button) myDialog.findViewById(R.id.btnSendComment);
        txtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDialog.dismiss();
            }
        });
//        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        buttonSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createComment();
            }
        });
        myDialog.show();
    }

    private void createComment() {
        String comment = editComment.getText().toString().trim();

        if (TextUtils.isEmpty(comment)) {
            editComment.setError("Please enter you comment");
            editComment.requestFocus();
            return;
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("comment", comment);

        //Memanggil create Mahasiswa API
        PerformNetworkRequest request = new PerformNetworkRequest("http://192.168.1.101/aselisurabaya3/aselisurabaya/api/apiComment.php?apicall=create_comment",
                params, CODE_POST_REQUEST);
        request.execute();
    }

    private class PerformNetworkRequest extends AsyncTask<Void, Void, String> {
        String url;

        HashMap<String, String> params;


        int requestCode;


        PerformNetworkRequest(String url, HashMap<String, String> params, int
                requestCode) {
            this.url = url;
            this.params = params;
            this.requestCode = requestCode;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressBar.setVisibility(View.GONE);

            try {
                JSONObject object = new JSONObject(s);
                if (!object.getBoolean("error")) {
                    Toast.makeText(getApplicationContext(),
                            object.getString("message"), Toast.LENGTH_LONG).show();
                    refreshMahasiswaList(object.getJSONArray("comment"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... voids) {
            RequestHandler requestHandler = new RequestHandler();

            if (requestCode == CODE_POST_REQUEST)
                return requestHandler.sendPostRequest(url, params);
            return null;
        }
    }

    public void refreshMahasiswaList(JSONArray mahasiswa) throws JSONException {
        list_data.clear();
        for (int i = 0; i < mahasiswa.length() ; i++) {
            JSONObject obj = mahasiswa.getJSONObject(i);

//            list_data.add(new Comment(
//                    obj.getInt("id_comment"),
//                    obj.getInt("id_wisata"),
//                    obj.getInt("id_user"),
//                    obj.getString("comment")
//            ));
        }

        AdapterListComment adapter = new AdapterListComment(CommentActivity.this,list_data);
        lvComment.setAdapter(adapter);
    }
}
