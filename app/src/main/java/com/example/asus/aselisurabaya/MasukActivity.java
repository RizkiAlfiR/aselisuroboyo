//package com.example.lenovo.myapplication;
//
//import android.app.ProgressDialog;
//import android.content.Intent;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import com.example.asus.aselisurabaya.R;
//import com.example.lenovo.myapplication.api.ApiService;
//import com.example.lenovo.myapplication.model.MasukModel;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class MasukActivity extends AppCompatActivity {
//    private Button btnMasuk, btnDaftar;
//    private EditText etmEmail, etmPassword;
//    String semail, spassword;
//    ProgressDialog loading;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_masuk);
//
//        etmEmail=(EditText)findViewById(R.id.etm_email);
//        etmPassword=(EditText)findViewById(R.id.etm_pass);
//        btnMasuk=(Button)findViewById(R.id.btn_masuk);
//        btnDaftar=(Button)findViewById(R.id.btn_daftar);
//        btnMasuk.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                semail = etmEmail.getText().toString();
//                spassword = etmPassword.getText().toString();
//                if (!semail.isEmpty() && !spassword.isEmpty())
//                {
//                    doSignIn(semail,spassword);
//                }else {
//                    Toast.makeText(MasukActivity.this, "data kosong", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });
//        btnDaftar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent daftarIntent = new Intent(MasukActivity.this, DaftarActivity.class);
//                startActivity(daftarIntent);
//            }
//        });
//
//
//    }
//
//    private void doSignIn(String email, String password) {
//        loading = new ProgressDialog(MasukActivity.this, R.style.Theme_AppCompat_DayNight_Dialog);
//        loading.setMessage("Authenticating..");
//        loading.show();
//
//        ApiService.service_post.postLogin("Application/json",email,password).enqueue(new Callback<MasukModel>() {
//            @Override
//            public void onResponse(Call<MasukModel> call, Response<MasukModel> response) {
//                if (response.isSuccessful())
//                {
//                    loading.dismiss();
//                    Toast.makeText(MasukActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    Intent dashboard = new Intent(MasukActivity.this, MainActivity.class);
//                    startActivity(dashboard);
//                    finish();
//
//                }
//                else
//                {
//                    //Toast.makeText(MasukActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    Toast.makeText(MasukActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    loading.dismiss();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<MasukModel> call, Throwable t) {
//
//            }
//        });
//    }
//}
