package com.example.asus.aselisurabaya;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.asus.aselisurabaya.api.ApiService;
import com.example.asus.aselisurabaya.model.DaftarModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {
    private Button btnDaftarFinal;
    private EditText etdUsername, etdPassword, etdBirth, etdEmail;
    String sdusername, sdpassword, sdbirth, sdemail;
    ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        etdUsername=(EditText)findViewById(R.id.etDUsername);
        etdPassword=(EditText)findViewById(R.id.etDPassword);
        etdBirth=(EditText)findViewById(R.id.etDBirth);
        etdEmail=(EditText)findViewById(R.id.etDEmail);

        btnDaftarFinal=(Button)findViewById(R.id.btnSignUpFinal);
        btnDaftarFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sdusername= etdUsername.getText().toString();
                sdpassword= etdPassword.getText().toString();
                sdbirth = etdBirth.getText().toString();
                sdemail = etdEmail.getText().toString();
                if (!sdusername.isEmpty() && !sdpassword.isEmpty() && !sdbirth.isEmpty() && !sdpassword.isEmpty())
                {
                    doSignUp(sdusername, sdpassword, sdbirth, sdemail);
                }else {
                    Toast.makeText(com.example.asus.aselisurabaya.SignUpActivity.this, "Data belum terisi", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button buttonLoginPage = (Button) findViewById(R.id.btnLoginPage);
        buttonLoginPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void doSignUp(String username, String password, String birth, String email) {
        loading = new ProgressDialog(com.example.asus.aselisurabaya.SignUpActivity.this, R.style.Theme_AppCompat_DayNight_Dialog);
        loading.setMessage("Authenticating..");
        loading.show();

        ApiService.service_post.postRegister("Application/json", username, password, birth, email).enqueue(new Callback<DaftarModel>() {
            @Override
            public void onResponse(Call<DaftarModel> call, Response<DaftarModel> response) {
                if (response.isSuccessful())
                {
                    loading.dismiss();
                    Toast.makeText(com.example.asus.aselisurabaya.SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent dashboard = new Intent(com.example.asus.aselisurabaya.SignUpActivity.this, MainActivity.class);
                    startActivity(dashboard);
                    finish();
                }
                else
                {
                    Toast.makeText(com.example.asus.aselisurabaya.SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    loading.dismiss();
                }
            }

            @Override
            public void onFailure(Call<DaftarModel> call, Throwable t) {

            }
        });

    }

}
