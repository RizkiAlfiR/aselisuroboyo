package com.example.asus.aselisurabaya;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ASUS on 6/23/2018.
 */

public class AdapterListBelanja extends RecyclerView.Adapter<AdapterListBelanja.ViewHolder> {
    Context context;
    ArrayList<HashMap<String, String>> list_data;

    public AdapterListBelanja(WisataBelanjaActivity wisataBelanjaActivity, ArrayList<HashMap<String, String>> list_data) {
        this.context = wisataBelanjaActivity;
        this.list_data = list_data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_wisata2, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Glide.with(context)
                .load("http://192.168.1.101/aselisurabaya3/aselisurabaya/upload/" + list_data.get(position).get("gambar"))
                .crossFade()
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.imgWisata);
        holder.txtNama.setText(list_data.get(position).get("nama_wisata"));
        //holder.txtRating.setText(list_data.get(position).get("like"));
        holder.txtKategori.setText(list_data.get(position).get("jenis_wisata"));
        holder.txtDescription.setText(list_data.get(position).get("deskripsi"));
        holder.txtAlamat.setText(list_data.get(position).get("lokasi"));
        holder.txtBuka.setText(list_data.get(position).get("buka"));
        holder.txtTutup.setText(list_data.get(position).get("tutup"));
        holder.buttonComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CommentActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgWisata;
        RatingBar ratingWisata;
        TextView txtNama, txtRating, txtKategori, txtDescription, txtAlamat, txtBuka, txtTutup;
        Button buttonComment;

        public ViewHolder(View itemView) {
            super(itemView);

            buttonComment = (Button) itemView.findViewById(R.id.btnComment);
            txtNama = (TextView) itemView.findViewById(R.id.textViewNama);
            //txtRating = (TextView) itemView.findViewById(R.id.textViewRating);
            txtKategori = (TextView) itemView.findViewById(R.id.textViewKategori);
            txtDescription = (TextView) itemView.findViewById(R.id.textViewDescription);
            txtAlamat = (TextView) itemView.findViewById(R.id.textViewAlamat);
            txtBuka = (TextView) itemView.findViewById(R.id.textViewJamBuka);
            txtTutup = (TextView) itemView.findViewById(R.id.textViewJamTutup);
            imgWisata = (ImageView) itemView.findViewById(R.id.imageViewWisata);
        }
    }
}
