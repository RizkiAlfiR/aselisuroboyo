package com.example.asus.aselisurabaya;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class WisataAlamActivity extends AppCompatActivity {
    private RecyclerView lvWisataAlam;
    private RequestQueue requestQueue;
    private StringRequest stringRequest;

    ArrayList<HashMap<String, String>> list_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wisata_alam);

        String url = "http://192.168.1.101/aselisurabaya3/aselisurabaya/api/apiWisata.php?apicall=get_wisata_perjenis&jenis=Wisata Alam"; //url API

        lvWisataAlam = (RecyclerView) findViewById(R.id.lvWisataAlam);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        lvWisataAlam.setLayoutManager(llm);

        requestQueue = Volley.newRequestQueue(WisataAlamActivity.this);

        list_data = new ArrayList<HashMap<String, String>>();

        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("response ", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("wisata");
                    for (int a = 0; a < jsonArray.length(); a++) {
                        JSONObject json = jsonArray.getJSONObject(a);
                        HashMap<String, String> map = new HashMap<String, String>();
                        //kolom-kolom
                        map.put("id", json.getString("id_wisata"));
                        map.put("nama_wisata", json.getString("nama_wisata"));
                        map.put("max_harga", json.getString("max_harga"));
                        map.put("min_harga", json.getString("min_harga"));
                        map.put("deskripsi", json.getString("deskripsi"));
                        map.put("lokasi", json.getString("lokasi"));
                        map.put("buka", json.getString("buka"));
                        map.put("tutup", json.getString("tutup"));
                        map.put("jenis_wisata", json.getString("jenis_wisata"));
                        map.put("like", json.getString("rating_like"));
                        map.put("dislike", json.getString("rating_dislike"));
                        map.put("gambar", json.getString("gambar"));
                        map.put("tanggal", json.getString("tgl_pembuatan"));
                        list_data.add(map);
                        AdapterListAlam adapter = new AdapterListAlam(WisataAlamActivity.this, list_data);
                        lvWisataAlam.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(WisataAlamActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(stringRequest);
    }
}
