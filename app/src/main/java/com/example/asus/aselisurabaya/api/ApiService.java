package com.example.asus.aselisurabaya.api;

/**
 * Created by ASUS on 5/28/2018.
 */

        import com.example.asus.aselisurabaya.model.DaftarModel;
        import com.example.asus.aselisurabaya.model.MasukModel;

        import retrofit2.Call;
        import retrofit2.Retrofit;
        import retrofit2.converter.gson.GsonConverterFactory;
        import retrofit2.http.Field;
        import retrofit2.http.FormUrlEncoded;
        import retrofit2.http.Header;
        import retrofit2.http.Headers;
        import retrofit2.http.POST;

public class ApiService {
    //Untuk menyambungkan android dengan web server. android dan webserver harus adala satu jaringan
    public static String BASE_URL = "http://192.168.1.101/aselisurabaya3/aselisurabaya/api/";

    //Untuk mengirim data
    public static PostService service_post = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build().create(ApiService.PostService.class);

    public interface PostService {

        @FormUrlEncoded
        @POST("apiUser.php?apicall=login_user")
        Call<MasukModel> postLogin( @Header ("Accept") String header,
                                    @Field("username") String username,
                                    @Field("password") String password);


        @POST("apiUser.php?apicall=create_user")
        @FormUrlEncoded
        Call<DaftarModel> postRegister( @Header ("Accept") String header,
                                        @Field("username") String username,
                                        @Field("password") String password,
                                        @Field("birth") String birth,
                                        @Field("email") String email);

    }
}
