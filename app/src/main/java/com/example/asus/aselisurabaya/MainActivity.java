package com.example.asus.aselisurabaya;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton buttonAlam = (ImageButton) findViewById(R.id.btnAlam);
        buttonAlam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), WisataAlamActivity.class);
                startActivity(intent);
            }
        });

        ImageButton buttonBelanja = (ImageButton) findViewById(R.id.btnBelanja);
        buttonBelanja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), WisataBelanjaActivity.class);
                startActivity(intent);
            }
        });

        ImageButton buttonKuliner = (ImageButton) findViewById(R.id.btnKuliner);
        buttonKuliner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), WisataKulinerActivity.class);
                startActivity(intent);
            }
        });

        ImageButton buttonReligi = (ImageButton) findViewById(R.id.btnReligi);
        buttonReligi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), WisataReligiActivity.class);
                startActivity(intent);
            }
        });

        ImageButton buttonEdukasi = (ImageButton) findViewById(R.id.btnEdukasi);
        buttonEdukasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), WisataEdukasiActivity.class);
                startActivity(intent);
            }
        });

        ImageButton buttonLogout = (ImageButton) findViewById(R.id.btnLoguout);
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

    }

}
